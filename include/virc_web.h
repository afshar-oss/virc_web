
#ifndef _VIRC_WEB_H
#define _VIRC_WEB_H

#include "esp_http_client.h"
#include "virc_core.h"
#include "virc_wifi.h"
#include "virc_crypto.h"

#include "rawcollar.pb-c.h"

#define VIRC_WEB_TASK_STACK_SIZE (8192)

void virc_web_run(void);

#endif
