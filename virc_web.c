
#include "virc_web.h"

static const char *TAG = "virc-web";
static const char *TASK = "virc-web-task";

static TaskHandle_t web_task_handle = NULL;

static int64_t web_timer;

static Moggie__RawCollarPacket p = MOGGIE__RAW_COLLAR_PACKET__INIT;


static esp_err_t
on_client_event
(esp_http_client_event_t *evt)
{
    switch(evt->event_id) {
        case HTTP_EVENT_ERROR:
            ESP_LOGI(TAG, "HTTP_EVENT_ERROR");
            break;
        case HTTP_EVENT_ON_CONNECTED:
            ESP_LOGI(TAG, "HTTP_EVENT_ON_CONNECTED");
            break;
        case HTTP_EVENT_HEADER_SENT:
            ESP_LOGI(TAG, "HTTP_EVENT_HEADER_SENT");
            break;
        case HTTP_EVENT_ON_HEADER:
            ESP_LOGI(TAG, "HTTP_EVENT_ON_HEADER");
            ESP_LOGI(TAG, "%.*s", evt->data_len, (char*)evt->data);
            break;
        case HTTP_EVENT_ON_DATA:
            ESP_LOGI(TAG, "HTTP_EVENT_ON_DATA, len=%d", evt->data_len);
            if (!esp_http_client_is_chunked_response(evt->client)) {
                printf("%.*s", evt->data_len, (char*)evt->data);
            }
            break;
        case HTTP_EVENT_ON_FINISH:
            ESP_LOGI(TAG, "HTTP_EVENT_ON_FINISH");
            break;
        case HTTP_EVENT_DISCONNECTED:
            ESP_LOGI(TAG, "HTTP_EVENT_DISCONNECTED");
            break;
    }
    return ESP_OK;
}


bool
virc_web_request
(void)
{
  esp_http_client_config_t config = {
   .url = "http://httpbin.org/post",
   .event_handler = on_client_event,
   .user_agent = "MoggieWeb-1.0",
   .method = HTTP_METHOD_POST,
  };
  esp_http_client_handle_t client = esp_http_client_init(&config);


  #define SZ (500)
  Moggie__RawCollarVector v = MOGGIE__RAW_COLLAR_VECTOR__INIT;
  v.ax = 4.9f;
  p.n_vectors = SZ;
  p.vectors = malloc(sizeof(v) * SZ);
  for (uint16_t i = 0; i < SZ; i++) {
    p.vectors[i] = &v;
  }
  size_t len = moggie__raw_collar_packet__get_packed_size(&p);
  void *buf = malloc(len);
  moggie__raw_collar_packet__pack(&p, buf);
  uint8_t sig[128];
  virc_crypto_sign(buf, len, sig);
  ESP_LOGI(TAG, "data length(%d) signature(%s)", len, sig);

  esp_err_t err;

  err = esp_http_client_set_post_field(client, buf, len);
  if (err != ESP_OK) {
    ESP_LOGE(TAG, "failed esp_http_client_set_post_field (%s)", esp_err_to_name(err));
    return false;
  }
  ESP_LOGD(TAG, "ok esp_http_client_set_post_field");

  err = esp_http_client_set_header(client, "Content-Type", "text/plain");
  if (err != ESP_OK) {
    ESP_LOGE(TAG, "failed esp_http_client_set_header Content-Type (%s)", esp_err_to_name(err));
    return false;
  }
  ESP_LOGD(TAG, "ok esp_http_client_set_header Content-Type");

  err = esp_http_client_set_header(client, "X-Sig", (char *)sig);
  if (err != ESP_OK) {
    ESP_LOGE(TAG, "failed esp_http_client_set_header X-Sig (%s)", esp_err_to_name(err));
    return false;
  }
  ESP_LOGD(TAG, "ok esp_http_client_set_header X-Sig");


  //err = esp_http_client_perform(client);
  if (err != ESP_OK) {
    ESP_LOGE(TAG, "failed esp_http_client_perform (%s)", esp_err_to_name(err));
    return false;
  }
  ESP_LOGD(TAG, "ok esp_http_client_perform");
  

  free(p.vectors);
  free(buf);

  if (err == ESP_OK) {
    ESP_LOGI(TAG, "Status = %d, content_length = %d",
           esp_http_client_get_status_code(client),
           esp_http_client_get_content_length(client));
  }
  
  err = esp_http_client_cleanup(client);
  if (err != ESP_OK) {
    ESP_LOGE(TAG, "failed esp_http_client_cleanup (%s)", esp_err_to_name(err));
    return false;
  }
  ESP_LOGD(TAG, "ok esp_http_client_cleanup");
  return true;

}


static void
web_task
(void *pvParameters)
{
  virc_wifi_wait();
  virc_timer_reset(&web_timer);
  for (;;) {
    //if (virc_timer_has(&web_timer, 5000)) {
      
    //virc_web_request();
    DELAY(3000);
    //}
  }
}

void
virc_web_run
(void)
{
  xTaskCreate(web_task, TASK, VIRC_WEB_TASK_STACK_SIZE,
      NULL, tskIDLE_PRIORITY, &web_task_handle);
  
}
